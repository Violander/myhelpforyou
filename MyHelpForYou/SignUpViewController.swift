//
//  SignUpViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

//Gestisco i Textfield per la registrazione
//Controllo se l'utente è già registrato e loggato lo pusho nella home
//Inserisco la logica per registrare l'utente salvando i suoi valori importanti su firebase
//Gestico messaggi di errori nei seguenti casi:
//- Email Non Valida
//- Password Non Valida
//- Campi Vuoti

import UIKit
import FirebaseAuth

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    let TagName = 0
    let TagSurname = 1
    let TagEmail = 2
    let TagPassword = 3
    
    var name: String?
    var surname: String?
    var email: String?
    var password: String?
    
    var loadingView: UIView!
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var constraintTopStackViewTExtFields: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.instance.specialNavigationControleller = self.navigationController as! SpecialNavigationController
        //Create a loading view
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.loadingView.isHidden = false
        //Check if user is already logged and push to home screen
        if Auth.auth().currentUser != nil {
            AppDelegate.instance.currentUserUid = Auth.auth().currentUser?.uid
            self.performSegue(withIdentifier: Constants.Segues.GoToHome, sender: nil)
        } else {
            self.loadingView.isHidden = true
        }
        //Initialize Layout
        self.initializeLayout()
        //Add observer to keyboard to adjust textfields
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSignUpButtonTap() {
        //Stop Editing
        self.view.endEditing(true)
        //Start show loading indicator
        self.loadingView.isHidden = false
        //If Fields are not empty
        if self.validateItems() {
            //Signup User
            Auth.auth().createUser(withEmail: self.email!, password: self.password!) { (user, error) in
                if user != nil {
                    AppDelegate.instance.currentUserUid = user!.uid
                    AppDelegate.instance.ref.child("users/\(user!.uid)/name").setValue(self.name!)
                    AppDelegate.instance.ref.child("users/\(user!.uid)/surname").setValue(self.surname!)
                    AppDelegate.instance.ref.child("users/\(user!.uid)/type").setValue("caregiver")
                    AppDelegate.instance.ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
                        self.loadingView.isHidden = true
                        // Get user value
                        let value = snapshot.value as? NSDictionary
                        
                        let idNumber = "1\(value?.count ?? 0)"
                        AppDelegate.instance.ref.child("users/\(user!.uid)/idNumber").setValue(idNumber)
                        
                        AppDelegate.instance.ref.child("caregivers/members/\(idNumber)").setValue(user!.uid)
                        AppDelegate.instance.ref.child("users/\(user!.uid)/surname").setValue(self.surname!)
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                        }
                        self.performSegue(withIdentifier: Constants.Segues.GoToHome, sender: nil)
                    }) { (error) in
                        DispatchQueue.main.async {
                            self.loadingView.isHidden = true
                            let _ = SweetAlert().showAlert("", subTitle: error.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
                        }
                    }
                } else if error != nil {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: error!.localizedDescription, style: .error, buttonTitle: "Ok", action: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.loadingView.isHidden = true
                        let _ = SweetAlert().showAlert("", subTitle: "Failed To Sign In", style: .error, buttonTitle: "Ok", action: nil)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                let _ = SweetAlert().showAlert("", subTitle: "Empty Fields", style: .error, buttonTitle: "Ok", action: nil)
            }
        }
    }
    
    func initializeLayout() {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case TagName:
            self.name = textField.text
            break
        case TagSurname:
            self.surname = textField.text
            break
        case TagEmail:
            self.email = textField.text
            break
        case TagPassword:
            self.password = textField.text
            break
        default:
            break
        }
    }
    
    func validateItems() -> Bool {
        if self.name == nil || self.name == "" || self.surname == nil || self.surname == "" || self.email == nil || self.email == "" || self.password == nil || self.password == ""{
            return false
        }
        return true
    }
    
    
    //MARK: - Notifications
    @objc
    func keyboardWillShow(notification: NSNotification) {
        if self.constraintTopStackViewTExtFields.constant == 0 {
            self.constraintTopStackViewTExtFields.constant -= 90
            self.view.needsUpdateConstraints()
        }
    }
    
    @objc
    func keyboardWillHide(notification: NSNotification) {
        if self.constraintTopStackViewTExtFields.constant != 0{
            self.constraintTopStackViewTExtFields.constant = 0
            self.view.needsUpdateConstraints()
        }
    }
}
