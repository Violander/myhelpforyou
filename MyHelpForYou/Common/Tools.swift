//
//  Tools.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation
import UIKit

class Tools {
    
    static func loadingView(frame: CGRect) -> UIView {
        
        let loadingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        loadingIndicator.color = UIColor.darkGray
        loadingIndicator.startAnimating()
        
        loadingIndicator.frame = CGRect(x: frame.size.width/2 - loadingIndicator.frame.size.width/2, y: frame.size.height/2 - loadingIndicator.frame.size.height/2, width: loadingIndicator.frame.size.width, height: loadingIndicator.frame.size.height)
        let view = UIView(frame: frame)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        view.addSubview(loadingIndicator)
        view.isHidden = true
        return view
    }
}
