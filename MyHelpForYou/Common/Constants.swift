//
//  Constants.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Segues {
        static let GoToSignIn = "goToSignIn"
        static let GoToHome = "goToHome"
        static let ShowMainMenu = "showMainMenu"
        static let GoToNotificationArchive = "goToNotificationArchive"
        static let OpenPopupAddCaregiver = "openPopupAddCaregiver"
    }
    
    struct Cell {
        static let CareGiversCell = "PatientsCell"
    }
    
}
