//
//  CareGiversViewController.swift
//  HelpMe
//
//  Created by Romeo Violini on 21/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit
import Firebase

class PatientsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PatientsTableViewCellDelegate {
    
    let tagLinked = 0
    let tagPending = 1
    
    var patientsLinked: [String: String]!
    var patientsPending: [String: String]!
    
    var loadingView: UIView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Get My Data
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as! [String: AnyObject]
            let myIdNumber = value["idNumber"] as! String
            AppDelegate.instance.currentUserIdNumber = myIdNumber
        })
        self.loadingView = Tools.loadingView(frame: self.view.frame)
        self.view.addSubview(self.loadingView)
        self.patientsLinked = [:]
        self.patientsPending = [:]
        self.loadingView.isHidden = false
        self.tableView.allowsSelection = false
        //Get Patients Data
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? [String: AnyObject]
            
            self.patientsLinked = value?["linked"] as? [String: String] ?? [:]
            self.patientsPending = value?["linked"] as? [String: String] ?? [:]
            
            DispatchQueue.main.async {
                self.loadingView.isHidden = true
                self.tableView.reloadData()
            }
        })
        //Observing Patients Data
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("linked").observe(.childAdded) { (snapshot) in
            //let dic =  [snapshot.key: snapshot.value as! String]
            self.patientsLinked[snapshot.key] = snapshot.value as? String
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("pending").observe(.childAdded) { (snapshot) in
            self.patientsPending[snapshot.key] = snapshot.value as? String
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        AppDelegate.instance.ref.child("users").child(AppDelegate.instance.currentUserUid!).child("pending").observe(.childRemoved) { (snapshot) in
            
            let key = snapshot.key
            self.patientsPending.removeValue(forKey: key)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        //Initialize Layout
        self.initializeLayout()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initializeLayout() {
        self.segmentedControl.removeBorders()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.segmentedControl.selectedSegmentIndex == tagLinked {
            return self.patientsLinked.count
        } else {
            return self.patientsPending.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Configure Cell With Patients informations
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.CareGiversCell, for: indexPath) as! PatientsTableViewCell
        cell.loadingIndicator.isHidden = false
        cell.labelIDNumber.isHidden = true
        cell.labelDisplayName.isHidden = true
        var patientUUID = ""
        
        if self.segmentedControl.selectedSegmentIndex == tagLinked {
            patientUUID = self.patientsLinked[Array(self.patientsLinked.keys)[indexPath.row]]!
            cell.patientIdNumber = Array(self.patientsLinked.keys)[indexPath.row]
        } else {
            patientUUID = self.patientsPending[Array(self.patientsPending.keys)[indexPath.row]]!
            cell.patientIdNumber = Array(self.patientsPending.keys)[indexPath.row]
        }
        AppDelegate.instance.ref.child("users").child(patientUUID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            let patient = snapshot.value as? [String: AnyObject]
            cell.delegate = self
            
            DispatchQueue.main.async {
                
                cell.labelDisplayName.text = (patient!["name"] as! String) + " " + (patient!["surname"] as! String)
                cell.labelIDNumber.text = "IDNumber: " + (patient!["idNumber"] as! String)
                cell.labelIDNumber.isHidden = false
                cell.labelDisplayName.isHidden = false
                cell.loadingIndicator.isHidden = true
                cell.loadingIndicator.isHidden = true
                if self.segmentedControl.selectedSegmentIndex == self.tagLinked {
                    cell.buttonAdd.isHidden = true
                    cell.buttonRemove.isHidden = true
                } else {
                    cell.buttonAdd.isHidden = false
                    cell.buttonRemove.isHidden = false
                }
            }
        })
        return cell
    }
    
    @IBAction func onSegmnentedControlChangeValue() {
        self.tableView.reloadData()
    }
    //Accept the request of a User and Add as Patient
    func patientTableViewCellOnAddTap(patientIdNumber: String) {
        self.loadingView.isHidden = false
        let patientUUID = self.patientsPending[patientIdNumber]
        let myCaregiverIdNumber = AppDelegate.instance.currentUserIdNumber
        let myCaregiverUUID = AppDelegate.instance.currentUserUid
        
        AppDelegate.instance.ref.child("users/\(myCaregiverUUID!)/pending/\(patientIdNumber)").setValue(nil)
        
        AppDelegate.instance.ref.child("users/\(patientUUID!)/pending/\(myCaregiverIdNumber!)").setValue(nil)
        
        AppDelegate.instance.ref.child("users/\(myCaregiverUUID!)/linked/\(patientIdNumber)").setValue(patientUUID!)
        
        AppDelegate.instance.ref.child("users/\(patientUUID!)/linked/\(myCaregiverIdNumber!)").setValue(myCaregiverUUID!)
        
        Messaging.messaging().subscribe(toTopic: patientIdNumber)
        self.loadingView.isHidden = true
    }
    //Decline Request and Remove from pending list
    func patientTableViewCellOnRemoveTap(patientIdNumber: String) {
        print("Remove Tap: \(patientIdNumber)")
        self.loadingView.isHidden = true
        let patientUUID = self.patientsPending[patientIdNumber]
        let myCaregiverIdNumber = AppDelegate.instance.currentUserIdNumber
        let myCaregiverUUID = AppDelegate.instance.currentUserUid
        AppDelegate.instance.ref.child("users/\(myCaregiverUUID!)/pending/\(patientIdNumber)").setValue(nil)
        AppDelegate.instance.ref.child("users/\(patientUUID!)/pending/\(myCaregiverIdNumber!)").setValue(nil)
        self.loadingView.isHidden = false
    }
}
