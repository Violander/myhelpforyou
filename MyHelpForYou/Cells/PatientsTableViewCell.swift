//
//  PatientsTableViewCell.swift
//  MyHelpForYou
//
//  Created by Romeo Violini on 23/11/17.
//  Copyright © 2017 Vioo. All rights reserved.
//

import UIKit

protocol PatientsTableViewCellDelegate: class {
    
    func patientTableViewCellOnAddTap(patientIdNumber: String)
    func patientTableViewCellOnRemoveTap(patientIdNumber: String)
}

class PatientsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelDisplayName: UILabel!
    @IBOutlet weak var labelIDNumber: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    weak var delegate: PatientsTableViewCellDelegate?
    
    var patientIdNumber: String!
    
    @IBOutlet weak var buttonRemove: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func onAddTap() {
        self.delegate?.patientTableViewCellOnAddTap(patientIdNumber: self.patientIdNumber)
    }
    
    @IBAction func onRemoveTap() {
        self.delegate?.patientTableViewCellOnRemoveTap(patientIdNumber: self.patientIdNumber)
    }
    
}
